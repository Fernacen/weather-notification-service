import requests
import boto3
from pygame import mixer
import time as t
from datetime import datetime
import re


mixer.pre_init(44100, -16, 2, 2048)

polly = boto3.Session(
    aws_access_key_id="###",
    aws_secret_access_key="###",
    region_name='us-west-2').client('polly')

def main():
    dateTimeObj = datetime.now()
    dictAlert = requests.get("https://api.weather.gov/alerts?zone=SCZ030").json()
    dictAlert = dictAlert['features']

    for item in dictAlert:
        item = item['properties']
        event = item['event']
        effective = item['effective']
        expiry = item['expires']
        desc = item['description']
        if item['effective'][0:10] <= dateTimeObj.strftime("%Y-%m-%d") and item['expires'][11:19] >= dateTimeObj.strftime("%H:%M:%S"):
            if item['event'].find('warning') == True:
                chime("warn")
            else:
                chime("attn")

            print(event,"\n", effective,"\n",expiry,"\n", desc, "\n")

            '''to find expiration
            substrexp = item['expires'][item['expires'].find("T") + 1:item['expires'].find(":")+3].replace(":", " ")'''

            outputTTS(item['event'], 'event')

            times = re.findall('([0-9 ]\d{3})\s(AM|PM)', item['description'])
            for tplTimePhrase in times:
                item['description'] = replaceText(item['description'], tplTimePhrase[0], "Time")

            outputTTS(item['description'], 'description')

            chime("end")

        else:
            forcast()

def forcast():

    dictForecast = requests.get("https://api.weather.gov/gridpoints/CAE/30,48/forecast").json()
    dictForecast = dictForecast['properties']
    dictForecast = dictForecast['periods']

    chime("chime")
    for item in dictForecast:
        print(item['name'])
        outputTTS(item['name'], 'name')

        print(item['detailedForecast'])
        outputTTS(item['detailedForecast'], 'detailedForecast')


def replaceText(strOriginal, strFind, strType):
    strChange = ""
    if strType == "Time":
        strChange = strFind[:2] + ":" + strFind[2:]

    return strOriginal.replace(strFind, strChange)

def outputTTS(strText, strType):
    tts = polly.synthesize_speech(Text=strText,
                                  OutputFormat='mp3',
                                  VoiceId='Matthew')
    if "AudioStream" in tts:
        file = open("Folder\You\Save\\" + strType + ".mp3", 'wb')
        file.write(tts['AudioStream'].read())
        file.close()

    mixer.init()
    mixer.music.load("Folder\You\Save\\" + strType + ".mp3")
    mixer.music.play()
    while mixer.music.get_busy() == True:
        pass
    mixer.quit()
    t.sleep(.25)

def chime(strType):
    mixer.init()
    if strType == "end":
        mixer.music.load("Folder\You\Save\\EASEND.mp3")
    elif strType == "attn":
        mixer.music.load("Folder\You\Save\\EASATTN.mp3")
    elif strType == "warn":
        mixer.music.load("Folder\You\Save\\EASWARN.mp3")
    elif strType == "chime":
        mixer.music.load("Folder\You\Save\\CHIME.mp3")
    mixer.music.play()
    while mixer.music.get_busy() == True:
        pass
    mixer.quit()
    t.sleep(.5)
main()